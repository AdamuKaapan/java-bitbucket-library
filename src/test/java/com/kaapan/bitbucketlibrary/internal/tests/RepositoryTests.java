package com.kaapan.bitbucketlibrary.internal.tests;

import com.kaapan.bitbucketlibrary.Repository;
import com.kaapan.bitbucketlibrary.Team;
import com.kaapan.bitbucketlibrary.User;
import java.io.IOException;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RepositoryTests {

    private final OAuthConsumer consumer;

    private static final String CONSUMER_KEY = "uyQQeVbVAPasLuv53q",
            CONSUMER_SECRET = "EPkSTfuEqXRfeLuFv9BPpJ7EWkY8zjeW";

    private static final String ACCESS_TOKEN = "2yrYjZEw3K65A37Csm",
            TOKEN_SECRET = "9CzqJAvd5cAg5VCwMUWEkRrLNZurvuQY";

    public RepositoryTests() {
        consumer = new DefaultOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        consumer.setTokenWithSecret(ACCESS_TOKEN, TOKEN_SECRET);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void myRepositoryTest() {
        try {
            Repository repo = Repository.get(consumer, "adamukaapan", "syncframeworktest");
        } catch (IOException ex) {
            Assert.fail("Exception thrown!");
        }
    }
}
