package com.kaapan.bitbucketlibrary.internal.tests;

import com.kaapan.bitbucketlibrary.User;
import java.io.IOException;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UserTests {

    private final OAuthConsumer consumer;

    private static final String CONSUMER_KEY = "uyQQeVbVAPasLuv53q",
            CONSUMER_SECRET = "EPkSTfuEqXRfeLuFv9BPpJ7EWkY8zjeW";

    private static final String ACCESS_TOKEN = "2yrYjZEw3K65A37Csm",
            TOKEN_SECRET = "9CzqJAvd5cAg5VCwMUWEkRrLNZurvuQY";

    public UserTests() {
        consumer = new DefaultOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        consumer.setTokenWithSecret(ACCESS_TOKEN, TOKEN_SECRET);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void myUserTest() {
        try {
            User user = User.get(consumer, "AdamuKaapan");
            assertEquals("AdamuKaapan", user.getUsername());
            assertEquals("", user.getWebsite());
            assertEquals("Adam Krpan", user.getDisplayName());
            assertEquals("{84d53a8c-0f9b-4074-a090-2868676f6fea}", user.getUuid());
            assertEquals(ISODateTimeFormat.dateTime().parseDateTime("2014-01-07T01:18:09.397143+00:00"), user.getCreatedOn());
            assertEquals("", user.getLocation());
        } catch (IOException ex) {
            Assert.fail("Exception thrown!");
        }
    }
}
