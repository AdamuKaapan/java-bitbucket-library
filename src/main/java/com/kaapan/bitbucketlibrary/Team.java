package com.kaapan.bitbucketlibrary;

import com.google.gson.Gson;
import com.kaapan.bitbucketlibrary.internal.BB_Team;
import com.kaapan.bitbucketlibrary.internal.BB_User;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Represents a Bitbucket team.
 *
 * @author Adam
 */
public class Team {

    public static final String teamsEndpoint = "https://api.bitbucket.org/2.0/teams/";

    private final String username;
    private final String website;
    private final String displayName;
    private final String uuid;
    private final DateTime createdOn;
    private final String location;
    private final BB_Team.TeamLinks links;

    public Team(String username, String website, String displayName, String uuid, String createdOn, String location) {
        this.username = username;
        this.website = website;
        this.displayName = displayName;
        this.uuid = uuid;
        this.createdOn = ISODateTimeFormat.dateTime().parseDateTime(createdOn);
        this.location = location;
        this.links = null;
    }

    public Team(String username, String website, String displayName, String uuid, String createdOn, String location, BB_Team.TeamLinks links) {
        this.username = username;
        this.website = website;
        this.displayName = displayName;
        this.uuid = uuid;
        this.createdOn = ISODateTimeFormat.dateTime().parseDateTime(createdOn);
        this.location = location;
        this.links = links;
    }

    public String getUsername() {
        return username;
    }

    public String getWebsite() {
        return website;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUuid() {
        return uuid;
    }

    public DateTime getCreatedOn() {
        return createdOn;
    }

    public String getLocation() {
        return location;
    }

    public static Team get(OAuthConsumer consumer, String username) throws IOException {
        try {
            URL url = new URL(teamsEndpoint + username);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();

            consumer.sign(request);

            request.connect();

            String str = "";

            Scanner scan = new Scanner(request.getInputStream());
            while (scan.hasNext()) {
                str += scan.nextLine();
            }
            
            Gson gson = new Gson();
            return gson.fromJson(str, BB_Team.class).toTeam();
        } catch (IOException | OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException e) {
            throw new IOException("Error getting data for user " + username, e);
        }
    }

    // TODO: Insert methods to get data from links!
}
