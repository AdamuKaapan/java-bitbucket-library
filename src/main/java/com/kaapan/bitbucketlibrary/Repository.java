package com.kaapan.bitbucketlibrary;

import com.google.gson.Gson;
import com.kaapan.bitbucketlibrary.internal.BB_Repository;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

/**
 * Represents a Bitbucket team.
 *
 * @author Adam
 */
public class Repository {

    public static final String repositoriesEndpoint = "https://api.bitbucket.org/2.0/repositories/";

    private String scm;
    private boolean has_wiki;
    private String description;
    private BB_Repository.RepositoryLinks links;
    private String fork_policy;
    private String name;
    private String language;
    private String created_on;
    private String full_name;
    private boolean has_issues;
    private String ownerLink;
    private String updated_on;
    private String size;
    private boolean is_private;
    private String uuid;

    public Repository(String scm, boolean has_wiki, String description, BB_Repository.RepositoryLinks links, String fork_policy, String name, String language, String created_on, String full_name, boolean has_issues, String ownerLink, String updated_on, String size, boolean is_private, String uuid) {
        this.scm = scm;
        this.has_wiki = has_wiki;
        this.description = description;
        this.links = links;
        this.fork_policy = fork_policy;
        this.name = name;
        this.language = language;
        this.created_on = created_on;
        this.full_name = full_name;
        this.has_issues = has_issues;
        this.ownerLink = ownerLink;
        this.updated_on = updated_on;
        this.size = size;
        this.is_private = is_private;
        this.uuid = uuid;
    }

    public static Repository get(OAuthConsumer consumer, String username, String repoSlug) throws IOException {
        try {
            URL url = new URL(repositoriesEndpoint + username + "/" + repoSlug);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();

            consumer.sign(request);

            request.connect();

            String str = "";

            Scanner scan = new Scanner(request.getInputStream());
            while (scan.hasNext()) {
                str += scan.nextLine();
            }
            
            Gson gson = new Gson();
            return gson.fromJson(str, BB_Repository.class).toRepository();
        } catch (IOException | OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException e) {
            throw new IOException("Error getting data for user " + username, e);
        }
    }

    public static String getRepositoriesEndpoint() {
        return repositoriesEndpoint;
    }

    public String getScm() {
        return scm;
    }

    public boolean isHas_wiki() {
        return has_wiki;
    }

    public String getDescription() {
        return description;
    }

    public BB_Repository.RepositoryLinks getLinks() {
        return links;
    }

    public String getFork_policy() {
        return fork_policy;
    }

    public String getName() {
        return name;
    }

    public String getLanguage() {
        return language;
    }

    public String getCreated_on() {
        return created_on;
    }

    public String getFull_name() {
        return full_name;
    }

    public boolean isHas_issues() {
        return has_issues;
    }

    public String getOwnerLink() {
        return ownerLink;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public String getSize() {
        return size;
    }

    public boolean isIs_private() {
        return is_private;
    }

    public String getUuid() {
        return uuid;
    }
    
    
}
