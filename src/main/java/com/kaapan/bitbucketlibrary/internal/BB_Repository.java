package com.kaapan.bitbucketlibrary.internal;

import com.kaapan.bitbucketlibrary.Repository;

/**
 * This is the internal class representing a Repositories (has ALL properties
 * from the JSON data, which is not necessary)
 *
 * @author AdamuKaapan
 */
public class BB_Repository {

    public class RepositoryLinks {

        public BB_HrefValues.HrefValue watchers, commits, html, avatar, forks, pullrequests;
        public BB_HrefValues.HrefNamePair[] clone;
    }

    public class RepositoryOwner {

        public class RepositoryOwnerLinks {

            public BB_HrefValues.HrefValue self, html, avatar;
        }

        public String username;
        public String display_name;
        public String uuid;
        public RepositoryOwnerLinks links;
    }

    public String scm;
    public boolean has_wiki;
    public String description;
    public RepositoryLinks links;
    public String fork_policy;
    public String name;
    public String language;
    public String created_on;
    public String full_name;
    public boolean has_issues;
    public RepositoryOwner owner;
    public String updated_on;
    public String size;
    public boolean is_private;
    public String uuid;
    
    public Repository toRepository()
    {
        return new Repository(scm, has_wiki, description, links, fork_policy, name, language, created_on, full_name, has_issues, owner.links.self.href, updated_on, size, is_private, uuid);
    }
}
