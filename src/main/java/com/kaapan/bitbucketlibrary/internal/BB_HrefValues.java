package com.kaapan.bitbucketlibrary.internal;

public class BB_HrefValues {
    public class HrefValue
    {
        public String href;
    }
    
    public class HrefNamePair
    {
        public String href, name;
    }
}
