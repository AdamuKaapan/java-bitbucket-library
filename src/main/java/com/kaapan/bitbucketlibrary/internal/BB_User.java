package com.kaapan.bitbucketlibrary.internal;

import com.kaapan.bitbucketlibrary.User;

/**
 * This is the internal class representing a User (has ALL properties from the
 * JSON data, which is not necessary)
 * @author AdamuKaapan
 */
public class BB_User {
    public class UserLinks
    {
        public BB_HrefValues.HrefValue self, repositories, html, followers, avatar, following;
    }
    
    public String username, website, display_name, uuid, created_on, location;
    public UserLinks links;
    
    public User toUser()
    {
        return new User(username, website, display_name, uuid, created_on, location, links);
    }
}
