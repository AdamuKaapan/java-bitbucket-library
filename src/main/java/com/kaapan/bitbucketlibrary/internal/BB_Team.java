package com.kaapan.bitbucketlibrary.internal;

import com.kaapan.bitbucketlibrary.Team;

/**
 * This is the internal class representing a Team (has ALL properties from the
 * JSON data, which is not necessary)
 * @author AdamuKaapan
 */
public class BB_Team {
    public class TeamLinks
    {
        public BB_HrefValues.HrefValue self, repositories, html, followers, avatar, members, following;
    }
    
    public String username, website, display_name, uuid, created_on, location;
    public TeamLinks links;
    
    public Team toTeam()
    {
        return new Team(username, website, display_name, uuid, created_on, location, links);
    }
}
